/* global DefAnimator*/
/* global PokolVoxelEngine_animator*/
/* global karakter1*/
/* global icontrol*/
/* global canv*/
/* global pve*/
/* global vec3*/

function $(id) {
    return document.getElementById(id);
}

//window.onbeforeunload = function () {return "Bzitosan bezárod a szerkesztőt?";};

function CreateScrollbar(kiir, nev, value, min, max, size, func, factor) {
    if (factor === undefined)
        factor = 1;
    var str = kiir + '<input style="width:40px; float: right;" id="kiir_' + nev + '" value="' + value + '"';
    str += ' onblur="this.setscroll();" onkeypress="var key=((\'which\' in event) ? event.which : event.keyCode)|0; if (key === 13) this.setscroll();"/><BR>';
    str += '<div style="margin-top: 10px;"><button style="vertical-align: super; width: 18px; height: 20px; padding: 0 0 0 0;" onclick="$(\'' + nev + '\').stepDown();$(\'' + nev + '\').oninput();">&lt;</button>';
    var atrb = 'min="' + min + '" max="' + max + '" value="' + value + '" style="width: ' + (size - 40) + 'px; height: 20px;" id="' + nev + '"';
    var mwheel = 'onmousewheel="if (((event.wheelDelta) ? (event.wheelDelta) : (-event.detail)) > 0) $(\'' + nev + '\').stepUp(); else $(\'' + nev + '\').stepDown(); $(\'' + nev + '\').oninput();"';
    str += '<input  type="range" ' + atrb + ' step="1"  oninput="$(\'kiir_' + nev + '\').value = ($(\'' + nev + '\').value*' + factor + ');' + func + '"  ' + mwheel + '/>';
    str += '<button style="vertical-align: super; width: 18px; height: 20px; padding: 0 0 0 0;" onclick="$(\'' + nev + '\').stepUp();$(\'' + nev + '\').oninput();">&gt;</button></div>';
    document.write(str);
    $("kiir_" + nev).setscroll = function () {
        SetScrollbar(nev, this.value);
        $(nev).oninput();
    };
    $(nev).factor = factor;
}
function SetScrollbar(id, value) {
    $(id).value = (value / $(id).factor) | 0;
    $("kiir_" + id).value = $(id).value * $(id).factor;
}

function MouseWheel_CB(e) {
    if (e > 0)
        Range++;
    if (e < 0)
        Range--;
    if (Range < 0)
        Range = 0;
}


var mopos = {x: 0, y: 0};
function Mouse_CB(e, movm, mpos) {

    if (GetPointerLock()) {

        icontrol.nezszog[0] += movm.y / 10.0;
        icontrol.nezszog[1] += movm.x / 10.0;
        mopos.x = canv.width >> 1;
        mopos.y = canv.height >> 1;
    } else {
        mopos = mpos;
    }
}
function UpdateTestList() {
    $("testlist").innerHTML = "";
    var testarr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].test;
    for (var i = 0; i < testarr.length; i++) {
        var obj = document.createElement("OPTION");
        obj.innerHTML = i + ". test";
        $("testlist").appendChild(obj);
    }
}

function UpdateKezList() {
    $("kezlist").innerHTML = "";
    var kezarr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez;
    for (var i = 0; i < kezarr.length; i++) {
        var obj = document.createElement("OPTION");
        obj.innerHTML = i + ". kéz";
        $("kezlist").appendChild(obj);
    }
}
function UpdateLabList() {
    $("lablist").innerHTML = "";
    var labarr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab;
    for (var i = 0; i < labarr.length; i++) {
        var obj = document.createElement("OPTION");
        obj.innerHTML = i + ". láb";
        $("lablist").appendChild(obj);
    }
}

function UpdateFejList() {
    $("fejlist").innerHTML = "";
    var fejarr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].fej;
    for (var i = 0; i < fejarr.length; i++) {
        var obj = document.createElement("OPTION");
        obj.innerHTML = i + ". fej";
        $("fejlist").appendChild(obj);
    }
}

function UpdateList() {
    UpdateTestList();
    UpdateKezList();
    UpdateLabList();
    UpdateFejList();
    UpdatePair();

}
function HideAllDiv() {
    $("guimenu_name").style.display = "none";
    $("guimenu_p").style.display = "none";
    $("guimenu_pos").style.display = "none";
    $("guimenu_m2").style.display = "none";
    $("guimenu_m1").style.display = "none";
    $("guimenu_m0").style.display = "none";
    $("guimenu_offset").style.display = "none";
    $("guimenu_rot").style.display = "none";
    $("guimenu_pair").style.display = "none";
    $("guimenu_anoffset").style.display = "none";
    $("div_pairfest").style.display = "none";
}
function ShowDivArr(arr) {
    var obj = getDefanimatorSelected();
    for (var i = 0; i < arr.length; i++) {
        var divnev = arr[i];
        $(divnev).style.display = "";
        switch (divnev) {
            case "guimenu_anoffset":
                var offsetToJoinPos = obj.offsetToJoinPos;
                SetScrollbar("sett_offsetToJoinPos", offsetToJoinPos);
                var offsetToTestPos = obj.offsetToTestPos;
                SetScrollbar("sett_offsetToJoinPosX", offsetToTestPos[0]);
                SetScrollbar("sett_offsetToJoinPosY", offsetToTestPos[1]);
                break;
            case "guimenu_name":
                $("sett_name").value = obj.name;
                break;
            case "guimenu_rot":
                if (obj.rot === undefined) {
                    SetScrollbar("sett_forg", 0);
                    $("chbox_autoforg").checked = true;
                } else {
                    SetScrollbar("sett_forg", obj.rot);
                    $("chbox_autoforg").checked = false;
                }
                break;
            case "guimenu_pos":
                if (obj.pos !== undefined) {
                    setArrowPos(0, obj.pos);


                } else {
                    setArrowPos(0, vec3.add([0, 0, 0], obj.r1pos, obj.offsetPos));
                    setArrowPos(1, vec3.add([0, 0, 0], obj.r2pos, obj.offsetPos));

                }
                break;
            case "guimenu_m0":
                if (obj.s0 !== undefined) {
                    SetScrollbar("sett_size0x", obj.s0[0]);
                    SetScrollbar("sett_size0z", obj.s0[1]);
                } else {
                    SetScrollbar("sett_size0x", obj.size[0]);
                    SetScrollbar("sett_size0z", obj.size[1]);

                    setArrowSize(0, (obj.size[0] + obj.size[1]) / 10);
                }
                break;
            case "guimenu_m1":
                setArrowSize(0, (obj.s1[0] + obj.s1[1]) / 5);
                SetScrollbar("sett_size1x", obj.s1[0]);
                SetScrollbar("sett_size1z", obj.s1[1]);
                break;
            case "guimenu_m2":
                setArrowSize(1, (obj.s2[0] + obj.s2[1]) / 5);
                SetScrollbar("sett_size2x", obj.s2[0]);
                SetScrollbar("sett_size2z", obj.s2[1]);
                break;
            case "guimenu_anoffset":
                SetScrollbar("sett_animoffset", obj.frameOffset);
                break;
            case "guimenu_pair":
                UpdatePair();
                var pairindex = obj.pair;
                if (pairindex === undefined) {
                    $("sel_pair").selectedIndex = 0;
                } else {
                    $("sel_pair").value = pairindex;
                }
                break;
            case "div_pairfest":
                if (obj.TexcoordPos) {
                    SetScrollbar("sett_xtexcoord", obj.TexcoordPos[0]);
                } else
                    SetScrollbar("sett_xtexcoord", 0);
                break;
        }
    }
}


function settEvent(e) {
    var value = e.value * e.factor;
    var arr = getDefanimatorSelected();
    if (e.id === "sett_offsetToJoinPosX") {

        arr.offsetToTestPos[0] = value;
        pve.animator.reSetTypeDataList();

    }
    if (e.id === "sett_offsetToJoinPosY") {

        arr.offsetToTestPos[1] = value;
        pve.animator.reSetTypeDataList();
    }

    if (e.id === "sett_offsetToJoinPos") {

        arr.offsetToJoinPos = value;


    }
    switch (e.id) {
        case "sett_size0x":
            if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {
                arr.s0[0] = value;
            } else if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ) {
                arr.size[0] = value;
            }
            break;
        case "sett_size0z":
            if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {
                arr.s0[1] = value;
            } else if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ) {
                arr.size[1] = value;
            }
            break;
        case "sett_size1x":
            arr.s1[0] = value;
            break;
        case "sett_size1z":
            arr.s1[1] = value;
            break;
        case "sett_size2x":
            arr.s2[0] = value;
            break;
        case "sett_size2z":
            arr.s2[1] = value;
            break;
        case "sett_forg":
            $("chbox_autoforg").checked = false;
            arr.rot = value;
            break;
        case "sett_animoffset":
            arr.frameOffset = value;
            break;
        case "sett_festr":
        case "sett_festg":
        case "sett_festb":
            paintCTX.fillStyle = "rgb(" + $("sett_festr").value + "," + $("sett_festg").value + "," + $("sett_festb").value + ")";
            break;
        case "sett_festm":
        case "sett_animspeed":
            break;
        case "sett_name":
            arr.name = $("sett_name").value;
            break;
        case "sett_xtexcoord":
            if (arr.TexcoordPos === undefined) {
                arr.TexcoordPos = [0, 0, 1, 1];
            }
            arr.TexcoordPos[2] = 1;
            if (value < 0) {
                arr.TexcoordPos[2] = -1;
            }
            arr.TexcoordPos[0] = value;
            break;
    }
    reSetAnimator();

}

function getSelectedDivName(index) {
    var divname = "";
    var bealmenu = [];
    switch (index) {
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST:
            divname = "tresz_test";
            bealmenu = ["guimenu_m0", "guimenu_pos", "guimenu_rot", "guimenu_p"];
            break;
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ:
            divname = "tresz_fej";
            bealmenu = ["guimenu_m0", "guimenu_pos", "guimenu_rot", "guimenu_p"];
            break;
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ:
            divname = "tresz_kez";
            bealmenu = ["guimenu_name", "guimenu_m0", "guimenu_anoffset", "guimenu_pair", "guimenu_m1", "guimenu_m2", "guimenu_offset", "guimenu_pos", "guimenu_p", "div_pairfest"];
            break;
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB:
            divname = "tresz_lab";
            bealmenu = ["guimenu_name", "guimenu_m0", "guimenu_anoffset", "guimenu_pair", "guimenu_m1", "guimenu_m2", "guimenu_offset", "guimenu_pos", "guimenu_p", "div_pairfest"];
            break;
    }
    return [divname, bealmenu];

}

function ChangeList(e) {
    kijelolIndex = e.selectedIndex;
    if (e.id === "kezlist") {
        kijelolType = PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ;
    } else if (e.id === "lablist") {
        kijelolType = PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB;
    } else if (e.id === "testlist") {
        kijelolType = PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST;
    } else if (e.id === "fejlist") {
        kijelolType = PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ;
    }
    var divret = getSelectedDivName(kijelolType);
    ShowDivArr(divret[1]);
    drawBackTextureToCanvas();

}

function chbox_ev(e) {
    var on = e.checked;
    switch (e.id) {
        case "chbox_fest":
        case "chbox_szinv":
            if (on) {

                karakter1.setAlpha(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.ALL, 0, 0.05);
                karakter1.setAlpha(kijelolType, kijelolIndex, 1);
            } else {
                karakter1.setAlpha(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.ALL, 0, 1);
            }
            break;
        case "chbox_pos":
            setArrowHidden(0, true);
            setArrowHidden(1, true);
            karakter1.setAlpha(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.ALL, 0, 1);

            if (on) {
                if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST) {
                    setArrowHidden(0, false);
                }
                if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {
                    setArrowHidden(0, false);
                    setArrowHidden(1, false);
                }
                karakter1.setAlpha(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.ALL, 0, 0.1);
                karakter1.setAlpha(kijelolType, kijelolIndex, 0.4);

            }
            break;
        case "chbox_autoforg":
            var arr = getDefanimatorSelected();
            var scroll = $("sett_forg");
            if (on) {
                arr.rot = undefined;
            } else {
                arr.rot = scroll.value * scroll.factor;
            }
            reSetAnimator();
            break;
    }

}
function SetAnimacio(animindex) {
    karakter1.setAnimAlg(animindex, 100);
}
function UpdatePair() {
    $("sel_pair").innerHTML = "";

    var arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab;
    if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ)
        arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez;
    var obj = document.createElement("OPTION");
    obj.value = -1;
    obj.innerHTML = "Nincs";
    $("sel_pair").appendChild(obj);

    for (var i = 0; i < arr.length; i++) {
        if (i === kijelolIndex) {
            continue;
        }
        var obj = document.createElement("OPTION");
        obj.value = i;
        obj.innerHTML = i + ". lab";
        if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {
            obj.innerHTML = i + ". kez";
        } else {
            obj.innerHTML = i + ". lab";
        }
        $("sel_pair").appendChild(obj);
    }

}

function SetPair() {
    var obj = getDefanimatorSelected();
    var index = parseInt($("sel_pair").value);
    obj.pair = index;
    reSetAnimator();
}
function guimenu_pevent(elem) {
    var eleme = $(elem.attributes.getNamedItem("data-id").value);
    if (eleme.attributes["data-small"] === undefined) {
        eleme.attributes.setNamedItem(document.createAttribute("data-small"));
        eleme.style.height = "17px";
        elem.innerHTML = "▼";
    } else {
        eleme.style.height = "";
        eleme.attributes.removeNamedItem("data-small");
        elem.innerHTML = "▲";
    }
}

var oldselect = 2;//test
function change_tresz() {

    $("testlist").selectedIndex = -1;
    $("kezlist").selectedIndex = -1;
    $("fejlist").selectedIndex = -1;
    $("lablist").selectedIndex = -1;


    var ret1 = getSelectedDivName(oldselect);
    $(ret1[0]).style.display = "none";
    HideAllDiv();

    oldselect = parseInt($("sel_testresz").value);
    var ret1 = getSelectedDivName(oldselect);
    $(ret1[0]).style.display = "";
    UpdatePair();
}

//////////////add///////////////////
function add_test() {
    var arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].test;
    var lastelem = arr[arr.length - 1];
    var newelem = {pos: vec3.add([0, 0, 0], lastelem.pos, [0, 1, 0]), size: [1, 1], rot: 0};
    arr.push(newelem);
    UpdateList();
    reSetAnimator();
}
function add_fej() {
    var arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].fej;
    var pos;
    if (arr.length === 0) {
        var testarr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].test;
        pos = vec3.copy([0, 0, 0], testarr[testarr.length - 1].pos);
        if (pos[1] < testarr[0].pos[1])
            pos = vec3.copy([0, 0, 0], testarr[0].pos);
        pos[1] -= 1;
    } else {
        var lastelem = arr[arr.length - 1];
        pos = lastelem.pos;
    }
    var newelem = {pos: vec3.add([0, 0, 0], pos, [0, 1, 0]), size: [1, 1], rot: 0};
    arr.push(newelem);
    UpdateList();
    reSetAnimator();

}

function szur_test() {

    var selectindex = $("testlist").selectedIndex;
    if (selectindex !== -1) {
        var arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].test;
        var nextelem = arr[selectindex].pos;
        var prevelem = vec3.add([0, 0, 0], nextelem, [0, -2, 0]);
        if (selectindex > 0)
            prevelem = arr[selectindex - 1].pos;
        var pos = vec3.scale([0, 0, 0], vec3.add([0, 0, 0], nextelem, prevelem), 0.5);
        var newelem = {pos: pos, size: [1, 1], rot: 0};
        arr.splice(selectindex, 0, newelem);
        UpdateList();
        reSetAnimator();
    }
}

function szur_fej() {
    var selectindex = $("fejlist").selectedIndex;
    if (selectindex !== -1) {
        var arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].fej;
        var nextelem = arr[selectindex].pos;
        var prevelem = vec3.add([0, 0, 0], nextelem, [0, -2, 0]);
        if (selectindex > 0)
            prevelem = arr[selectindex - 1].pos;
        var pos = vec3.scale([0, 0, 0], vec3.add([0, 0, 0], nextelem, prevelem), 0.5);
        var newelem = {pos: pos, size: [1, 1], rot: 0};
        arr.splice(selectindex, 0, newelem);
        UpdateList();
        reSetAnimator();
    }
}

function add_lab(on) {
    if (on) {
        $("add_lab_div").style.display = "";
        add_newlab = true;
    } else {
        $("add_lab_div").style.display = "none";
        add_newlab = false;
    }
}

function add_kez(on) {
    if (on) {
        $("add_kez_div").style.display = "";
        add_newkez = true;
    } else {
        $("add_kez_div").style.display = "none";
        add_newkez = false;
    }
}


///////////////////////////////torol///////////////
function torol_test() {
    var selectindex = $("testlist").selectedIndex;
    if (selectindex !== -1) {
        if (DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].test <= 2) {
            alert("Legalább 2 testrésznek maradnia kell!");
            return;
        }
        DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].test.splice(selectindex, 1);
        UpdateList();
        reSetAnimator();
    }
}

function torol_lab() {
    var selectindex = $("lablist").selectedIndex;
    if (selectindex !== -1) {
        var arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].pair === selectindex){
                arr[i].pair = undefined;
            }
        }
        arr.splice(selectindex, 1);
        UpdateList();
        reSetAnimator();
    }
}
function torol_kez() {
    var selectindex = $("kezlist").selectedIndex;
    if (selectindex !== -1) {
        var arr = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].pair === selectindex){
                arr[i].pair = undefined;
            }
        }
        arr.splice(selectindex, 1);
        UpdateList();
        reSetAnimator();
    }
}
function torol_fej() {
    var selectindex = $("fejlist").selectedIndex;
    if (selectindex !== -1) {
        DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].fej.splice(selectindex, 1);
        UpdateList();
        reSetAnimator();
    }
}
////////////////////ANIMATOR SAVE////////////
function animator_save() {
    var def_data_array = [];
    var def_data = unescape(encodeURIComponent(JSON.stringify(DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER], false, "\t")));
    for (var i = 0; i < def_data.length; i++) {
        def_data_array[i] = def_data.charCodeAt(i);
    }

    var zip = new JSZip();
    zip.file("defines.json", new Uint8Array(def_data_array), {binary: true});

    //save text
    var t = karakter1.getTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST, 0);
    if (t) {
        var data = pve.texman.getTexData(t);
        zip.file("tex_test.bin", data, {binary: true});
    }
    var t = karakter1.getTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ, 0);
    if (t) {
        var data = pve.texman.getTexData(t);
        zip.file("tex_fej.bin", data, {binary: true});
    }
    for (var i = 0; i < DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez.length; i++) {
        var t = karakter1.getTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ, i);
        if (t) {
            var data = pve.texman.getTexData(t);
            zip.file("tex_kez" + i + ".bin", data, {binary: true});
        }
    }

    for (var i = 0; i < DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab.length; i++) {
        var t = karakter1.getTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB, i);
        if (t) {
            var data = pve.texman.getTexData(t);
            zip.file("tex_lab" + i + ".bin", data, {binary: true});
        }
    }


    var content = zip.generate({type: "blob", compression: "DEFLATE", compressionOptions: {level: 6}});
    var letolt = document.createElement("a");
    letolt.href = URL.createObjectURL(content);
    letolt.download = "animator.zip";
    document.body.appendChild(letolt);
    letolt.click();
    document.body.removeChild(letolt);
}
function animator_load_cb(e) {
    var data = e.target.result;
    try
    {
        //zip decomp
        var zip = JSZip();
        zip.load(data);

        var deffile = zip.file("defines.json");
        var filedata = deffile.asUint8Array();

        var defines_str = decodeURIComponent(escape(String.fromCharCode.apply(null, filedata)));

        DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER] = JSON.parse(defines_str);
        UpdateList();
        reSetAnimator();

        var texfile = zip.file("tex_test.bin");
        if (texfile) {
            var data = texfile.asUint8Array();
            karakter1.setTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST, 0, pve.texman.getdatatex(paintCanvas.width, paintCanvas.height, data));
        }
        var texfile = zip.file("tex_fej.bin");
        if (texfile) {
            var data = texfile.asUint8Array();
            karakter1.setTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ, 0, pve.texman.getdatatex(paintCanvas.width, paintCanvas.height, data));
        }


        for (var i = 0; i < DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez.length; i++) {
            var texfile = zip.file("tex_kez" + i + ".bin");
            if (texfile) {
                var data = texfile.asUint8Array();
                karakter1.setTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ, i, pve.texman.getdatatex(paintCanvas.width, paintCanvas.height, data));
            }
        }

        for (var i = 0; i < DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab.length; i++) {
            var texfile = zip.file("tex_lab" + i + ".bin");
            if (texfile) {
                var data = texfile.asUint8Array();
                karakter1.setTexture(PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB, i, pve.texman.getdatatex(paintCanvas.width, paintCanvas.height, data));
            }
        }


    } catch (error) {
        alert("Zip error");
        throw error;
    }

}

function animator_load() {
    var input = document.createElement("INPUT");
    document.body.appendChild(input);
    input.type = "file";
    input.style.display = "none";
    input.onchange = function (event) {
        var file = event.target.files[0];
        if (!file) {
            return;
        }
        var reader = new FileReader();
        reader.onload = animator_load_cb;
        reader.onerror = function () {
            alert("fájl betöltés hiba");
        };
        reader.readAsBinaryString(file);
    };
    input.focus();
    input.click();

}