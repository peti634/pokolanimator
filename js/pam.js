/* global gl */
/* global DefAnimator*/
/* global PokolVoxelEngine_animator*/

var pve;
var DEBUG = true;
var gl_;
var canv;
var tm_canv;
var ctx;

var napfeny = 1.0;

var icontrol;
var Box;
var vilag;
var terrain;
var terrain2;

var kijelolIndex = -1;
var kijelolType = -1;//KARAKTER_OBJTYPE

var ObjSzerk;
var Range = 10;
var vvilag;

var box;
var plain;
var plain_tc;

var animbox1;
var animbox2;
var VoxelEpitTipus = 0;
var showvoxel = false;

var ViszonyPont;
var line;
var line2;
var line3;
var line4;
var joinlist = [];
var vertlist = [];
var sphere;

var sphere1;
var sphere2;
var sphere3;

var bone;
var karakter;
var colladaObj;
var tri1;
var tri2;

var TerepSzerkTipus = 0;
var ffbuffer;
var tatlas;
var ctoc;
var globalkaratlas;
var globalpos = [0, 6, 1.8];
var globaljoin = [0, 1];
var axisarrowobj = [];
var now_axisraypick = undefined;
var now_axisraypick_index = undefined;
var paintCanvas;
var paintCTX;

var add_newlab = false;
var add_newkez = false;

var texCollector = [];
var loadDataCollector = [];
var loadDataXhr = undefined;

var colladaList = [];
var obj3dList = [];


function getDefanimatorSelected() {
    switch (kijelolType) {
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ:
            return DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez[kijelolIndex];
            break;
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB:
            return DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab[kijelolIndex];
            break;
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST:
            return DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].test[kijelolIndex];
            break;
        case PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ:
            return DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].fej[kijelolIndex];
            break;
    }

    return undefined;
}

function reSetAnimator() {
    var obj = getDefanimatorSelected();
    if (obj && obj.pair !== undefined && obj.pair >= 0) {
        var pairobj = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez[obj.pair];
        if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB) {
            pairobj = DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab[obj.pair];
        }
        pairobj.offsetPos = vec3.mul([0, 0, 0], obj.offsetPos, [1, 1, -1]);
        pairobj.r1pos = vec3.mul([0, 0, 0], obj.r1pos, [1, 1, -1]);
        pairobj.r2pos = vec3.mul([0, 0, 0], obj.r2pos, [1, 1, -1]);
        pairobj.s0 = vec2.copy([0, 0], obj.s0);
        pairobj.s1 = vec2.copy([0, 0], obj.s1);
        pairobj.s2 = vec2.copy([0, 0], obj.s2);
        pairobj.joinTestIndex = obj.joinTestIndex;
        pairobj.offsetToJoinPos = obj.offsetToJoinPos;
        pairobj.offsetToTestPos = vec2.copy([0, 0], obj.offsetToTestPos);
    }
    pve.animator.reSetTypeDataList();
}

function Key_CB(key, up, dbclick) {
    if (up)
        return;
    if (key === INPUTKEYS.LBUTTON) {
        icontrol.MouseLock();
        return;
    }
    switch (key) {
        case INPUTKEYS.KEY_0:
            if ($("rad_10").checked)
                $("rad_11").checked = true;
            else
                $("rad_10").checked = true;
            break;
        case INPUTKEYS.KEY_1:
            $("rad_1").checked = true;
            break;
        case INPUTKEYS.KEY_2:
            $("rad_2").checked = true;
            break;
        case INPUTKEYS.KEY_3:
            $("rad_3").checked = true;
            break;
        case INPUTKEYS.KEY_4:
            $("rad_4").checked = true;
            break;
        case INPUTKEYS.KEY_5:
            $("rad_5").checked = true;
            break;
        case INPUTKEYS.KEY_Q:
            $("chbox_1").checked = !$("chbox_1").checked;
            break;
        case INPUTKEYS.KEY_T:
            $("chbox_3").checked = !$("chbox_3").checked;
            break;
        case INPUTKEYS.RBUTTON:
            now_axisraypick = undefined;

            var arrowpickobj = checkArrowRay();
            if (arrowpickobj) {
                now_axisraypick = arrowpickobj[1];
                now_axisraypick.startMouseMove();
                now_axisraypick_index = arrowpickobj[0];

            }


            break;
    }
    if (icontrol.key(INPUTKEYS.KEY_CTRL) && icontrol.key(INPUTKEYS.KEY_S)) {
        return true;
    }
}

function checkArrowRay() {
    var retobj = undefined;
    var maxlen = Number.POSITIVE_INFINITY;
    for (var i = 0; i < axisarrowobj.length; i++) {
        var arrowobj = axisarrowobj[i];

        if (arrowobj.xarrow.getHidden() === false) {
            var ret = arrowobj.xarrow.raypick();
            if (ret !== undefined) {
                var len = vec3.distance(ret, icontrol.pos);
                if (len < maxlen) {
                    retobj = [i, arrowobj.xarrow];
                    maxlen = len;

                }
            }
        }
        if (arrowobj.yarrow.getHidden() === false) {
            var ret = arrowobj.yarrow.raypick();
            if (ret !== undefined) {
                var len = vec3.distance(ret, icontrol.pos);
                if (len < maxlen) {
                    retobj = [i, arrowobj.yarrow];
                    maxlen = len;

                }
            }
        }
        if (arrowobj.zarrow.getHidden() === false) {
            var ret = arrowobj.zarrow.raypick();
            if (ret !== undefined) {
                var len = vec3.distance(ret, icontrol.pos);
                if (len < maxlen) {
                    retobj = [i, arrowobj.zarrow];
                    maxlen = len;

                }
            }
        }
    }
    return retobj;
}
function setArrowPos(index, pos) {
    pos = vec3.scale([0, 0, 0], pos, karakter1.getScale());
    vec3.add(pos, pos, karakter1.getPos());

    var arrowobj = axisarrowobj[index];
    arrowobj.xarrow.setPos(pos);
    arrowobj.yarrow.setPos(pos);
    arrowobj.zarrow.setPos(pos);

}
function setArrowSize(index, size) {
    size *= karakter1.getScale();
    var arrowobj = axisarrowobj[index];
    arrowobj.xarrow.setSize(size);
    arrowobj.yarrow.setSize(size);
    arrowobj.zarrow.setSize(size);
}
function setArrowHidden(i, hide) {

    var arrowobj = axisarrowobj[i];
    arrowobj.xarrow.setHidden(hide);
    arrowobj.yarrow.setHidden(hide);
    arrowobj.zarrow.setHidden(hide);

}

function upconvert() {

}

function convertfloat(s) {
    var ret = [];
    var arr = s.split(" ");
    for (var i = 0; i < arr.length; i++) {
        ret.push(parseFloat(arr[i]));
    }
    return ret;
}

function matrix_roworder(m) {
    function swap(i1, i2) {
        var temp = m[i1];
        m[i1] = m[i2];
        m[i2] = -temp;
    }
    //columns
    swap(4, 8);
    swap(5, 9);
    swap(6, 10);
    //rows
    swap(1, 2);
    swap(5, 6);
    swap(9, 10);
    //translation
    swap(7, 11);
}


function convert_colladamatrix(s) {
    var m = convertfloat(s);
    matrix_roworder(m);
    mat4.transpose(m, m);
    //mat4.scale(m, m, [10, 10, 10]);
    return m;
}

function loadData_cb(status, item, data) {
    console.info(status);
    console.info(item);

    var sdata = "";
    if (TextDecoder) {
        sdata = (new TextDecoder().decode(new Uint8Array(data)));
    } else {
        var udata = new Uint8Array(data);
        for (var i = 0; i < udata.length; i++) {
            sdata += String.fromCharCode(udata[i]);
        }
    }



    var colladaClass = pve.Load_Collada(sdata);
    colladaList.push(colladaClass);

    //find tex in texCollector
    for (var i = 0; i < colladaClass.meshlist.length; i++) {
        var mesh = colladaClass.meshlist[i];
        if (mesh.effect && mesh.effect.img) {
            var teximg = mesh.effect.img;
            teximg.texobj = texCollector[teximg.src];
        }
    }

    var collada_obj3d = new pve.animator.animatorInstance(colladaClass);
    obj3dList.push(collada_obj3d);
    
    
    var vegtagindex;
    /*
     vegtagindex = collada_obj3d.addVegtag(pve.animator.VEGTAGTYPE_LIST.LAB);
     
     collada_obj3d.setVegtagProp(vegtagindex, "TAG1", "Bone_015");
     collada_obj3d.setVegtagProp(vegtagindex, "TAG2", "Bone_016");
     collada_obj3d.setVegtagProp(vegtagindex, "TAG3", "Bone_017");
     collada_obj3d.setVegtagProp(vegtagindex, "RANGLE", Math.PI / 2);
     
     
     vegtagindex = collada_obj3d.addVegtag(pve.animator.VEGTAGTYPE_LIST.LAB);
     collada_obj3d.setVegtagProp(vegtagindex, "TAG1", "Bone_018");
     collada_obj3d.setVegtagProp(vegtagindex, "TAG2", "Bone_019");
     collada_obj3d.setVegtagProp(vegtagindex, "TAG3", "Bone_020");
     collada_obj3d.setVegtagProp(vegtagindex, "OFFSET", 0.5);
     collada_obj3d.setVegtagProp(vegtagindex, "RANGLE", Math.PI / 2);*/
     
     vegtagindex = collada_obj3d.addVegtag(pve.animator.VEGTAGTYPE_LIST.KEZ);
     collada_obj3d.setVegtagProp(vegtagindex, "TAG1", "Bone_008");
     collada_obj3d.setVegtagProp(vegtagindex, "TAG2", "Bone_009");
     collada_obj3d.setVegtagProp(vegtagindex, "TAG3", "Bone_010");
     collada_obj3d.setVegtagProp(vegtagindex, "OFFSET", 0.5);
     collada_obj3d.setVegtagProp(vegtagindex, "RANGLE", -Math.PI / 2);
     collada_obj3d.setVegtagProp(vegtagindex, "VANGLE", Math.PI / 2 - 0.1);
}
function loadDataXhr_cb() {
    if (loadDataXhr.readyState === 4) {
        var item = loadDataCollector[loadDataXhr.loadIndex];
        if (item.status !== 1) {
            console.error("loadDataXhr BUG!\r\nIndex: " + loadDataXhr.loadIndex + "\r\nUrl: " + item.url + "\r\nStatus: " + item.status);
        }
        loadData_cb(loadDataXhr.status, item, loadDataXhr.response);
        item.status = 2;
        for (var i = item.index; i < loadDataCollector.length; i++) {
            var newitem = loadDataCollector[i];
            if (newitem.status === 0) {
                loadDataXhr = new XMLHttpRequest();
                loadDataXhr.loadIndex = newitem.index;
                newitem.status = 1;
                loadDataXhr.open('GET', newitem.url, true);
                loadDataXhr.responseType = 'arraybuffer';
                loadDataXhr.onreadystatechange = loadDataXhr_cb;
                loadDataXhr.send();
                break;
            }

        }
    }
}
function addLoadData(url, type) {

    if (loadDataXhr === undefined)
        loadDataXhr = new XMLHttpRequest();
    var item = {
        url: url,
        type: type,
        index: loadDataCollector.length,
        status: 0//0 NEED LOAD, 1 LOADING, 2 READY
    };
    loadDataCollector.push(item);
    if (loadDataXhr.readyState === 0 || loadDataXhr.readyState === 4) {
        loadDataXhr.loadIndex = item.index;
        item.status = 1;
        loadDataXhr.open('GET', url, true);
        loadDataXhr.responseType = 'arraybuffer';
        loadDataXhr.onreadystatechange = loadDataXhr_cb;
        loadDataXhr.send();
    }
    return item;
}

function Start() {
    var utvonal_prefix = "img/";

    canv = $("glcanvas");
    pve = new PokolVoxelEngine();
    var sett = new PokolVoxelEngine_DefaultSettings();
    sett.canvas = canv;

    sett.render_maxrange = 500;
    sett.voxel_blokSsize = 4;
    sett.voxel_blokkOldalFeny = [0.78, 0.7, 0.4, 1.0, 0.6, 0.85];
    sett.voxel_terrainLengthXZ = 30;
    sett.blokkTypeList = BlockTypeList;
    sett.blokkCsoportList = CsoportTypeList;
    sett.url_prefix = utvonal_prefix;

    sett.callback_draw = frameTick;
    sett.callback_terraindraw = drawTerrain;
    sett.animator_animalg_list = DefAnimator.AnimAlg_List;
    sett.animator_typedata_list = DefAnimator.model_list;

    gl_ = pve.initGL(sett);

    if (gl_) {

        tatlas = pve.getTerrainAtlas();





        //ret.loadimg("img/moon.gif");"img/teszt.png"

        //addLoadData("obj3d/m4a1.dae");
        addLoadData("obj3d/soldier_bone.dae");


        paintCanvas = document.createElement("CANVAS");
        paintCanvas.width = 512;
        paintCanvas.height = 512;
        //$("tresz_test").appendChild(paintCanvas);
        paintCTX = paintCanvas.getContext("2d");
        paintCTX.fillStyle = "rgb(255,255,255)";
        paintCTX.fillRect(0, 0, 512, 512);




        //set def color (fekete)
        paintCTX.fillStyle = "rgb(0,0,0)";




        $("menu").style.display = "block";

        $("startbut").style.display = "none";


        icontrol = pve.inputControl(Mouse_CB, MouseWheel_CB, Key_CB);
        icontrol.LoadInputKey(INPUTKEYS);
        icontrol.SetPos(25, 15, 20);

        icontrol.SetNezszo(30, 160);

        vvilag = pve.voxelVilag;
        vvilag.addTerrain(0, 0);
        for (var i = 0; i < 30; i++) {
            for (var j = 0; j < 30; j++) {
                vvilag.addElem(i, 0, j, 1);
            }

        }
        vvilag.update();
        var tolpos = [-vvilag.blocksize / 2, 0, -vvilag.blocksize / 2];
        vvilag.setRelativePos(tolpos);



        sphere = new pve.geos.Geo_Sphere([0.1, 0.1, 0.1], [30, -30, 0], [1, 1, 1]);
        pve.geos.addList(sphere);

        sphere1 = new pve.geos.Geo_Sphere([0.1, 0.1, 0.1], [0, 0, 0], [1, 1, 1]);
        pve.geos.addList(sphere1);

        karakter1 = new pve.animator.Karakter(DefAnimator.ANIMALG_JAR, DefAnimator.TYPEDATA_EMBER);
        pve.animator.addKarakter(karakter1);
        karakter1.setScale(0.4);
        karakter1.setPos([30, 4, 30]);


        texCollector["m4_spec.tga"] = pve.texman.newtexture("img/m4_spec.png");
        texCollector["Tex1.jpg"] = pve.texman.newtexture("img/Tex1.jpg");

        axisarrowobj[0] = {
            xarrow: new pve.geos.Geo_AxisArrow(pve.geos.Geo_AxisArrow.XArrow, 0.4, 10, [0, 0, 0], [0, 1, 0]),
            yarrow: new pve.geos.Geo_AxisArrow(pve.geos.Geo_AxisArrow.YArrow, 0.4, 10, [0, 0, 0], [1, 0, 0]),
            zarrow: new pve.geos.Geo_AxisArrow(pve.geos.Geo_AxisArrow.ZArrow, 0.4, 10, [0, 0, 0], [0, 0, 1])
        };
        axisarrowobj[1] = {
            xarrow: new pve.geos.Geo_AxisArrow(pve.geos.Geo_AxisArrow.XArrow, 0.4, 10, [0, 0, 0], [0, 1, 0]),
            yarrow: new pve.geos.Geo_AxisArrow(pve.geos.Geo_AxisArrow.YArrow, 0.4, 10, [0, 0, 0], [1, 0, 0]),
            zarrow: new pve.geos.Geo_AxisArrow(pve.geos.Geo_AxisArrow.ZArrow, 0.4, 10, [0, 0, 0], [0, 0, 1])
        };
        pve.geos.addList(axisarrowobj[0].xarrow);
        pve.geos.addList(axisarrowobj[0].yarrow);
        pve.geos.addList(axisarrowobj[0].zarrow);
        pve.geos.addList(axisarrowobj[1].xarrow);
        pve.geos.addList(axisarrowobj[1].yarrow);
        pve.geos.addList(axisarrowobj[1].zarrow);
        setArrowHidden(0, true);
        setArrowHidden(1, true);


        UpdateList();
        pve.start();
        setInterval("kiirFPS()", 1000);



    }
}



function matrixTeszt(v)
{
    var m = mat4.create();
    mat4.rotateY(m, m, pve.math.degToRad(-icontrol.nezszog[1]));
    mat4.rotateX(m, m, pve.math.degToRad(-icontrol.nezszog[0]));


    if (v) {
        var m2 = mat4.create();
        var qu = quat.fromMat4(quat.create(), m);
        mat4.fromQuat(m2, qu);
        m = m2;
    }

    var mm = mat4.create();
    mat4.translate(mm, mm, icontrol.pos);
    mat4.mul(mm, mm, m);

    mat4.translate(mm, mm, [0, 0, -1]);
    sphere1.setExtMatrix(mm);

}

var pp = 0;


function drawBackTextureToCanvas() {
    var tex = karakter1.getTexture(kijelolType, kijelolIndex);
    if (tex === undefined) {
        var pixelarr = new Uint8Array(paintCanvas.width * paintCanvas.height * 4);
        for (var i = 0; i < pixelarr.length; i++) {
            pixelarr[i] = 255;
        }
        karakter1.setTexture(kijelolType, kijelolIndex, pve.texman.getdatatex(paintCanvas.width, paintCanvas.height, pixelarr));
        var imgd = new ImageData(512, 512);
        imgd.data.set(pixelarr);
        paintCTX.putImageData(imgd, 0, 0);
    } else {
        var data = pve.texman.getTexData(tex);
        var imgd = new ImageData(512, 512);
        imgd.data.set(data);
        paintCTX.putImageData(imgd, 0, 0);
    }


}
function getRaypPickIndex() {
    if (add_newlab || add_newkez) {
        return [PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST, 0];
    }

    if ($("chbox_fest").checked || $("chbox_szinv").checked) {
        if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST) {
            return [PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST, 0];
        } else if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ) {
            return [PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ, 0];
        } else if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB) {
            return [PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB, kijelolIndex];
        } else if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {
            return [PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ, kijelolIndex];
        }

    }
    if ($("chbox_pos").checked) {
        if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {
            return [PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST, 0];
        }
    }

    return undefined;
}

function raypickClick(testindex, point, tcoord) {
    var arr = getDefanimatorSelected();
    vec3.sub(point, point, karakter1.getPos());
    vec3.scale(point, point, 1 / karakter1.getScale());
    if (add_newlab) {
        var labobj = {
            s0: [0.8, 0.8],
            s1: [0.8, 0.8],
            s2: [0.8, 0.8],
            r1pos: [0.1, -5, 0],
            r2pos: [0, -10, 0],
            frameOffset: 0.5,
            offsetPos: point,
            offsetToJoinPos: 0.8,
            offsetToTestPos: [0.7, 0.1],
            joinTestIndex: testindex
        };
        DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].lab.push(labobj);
        add_lab(false);
        reSetAnimator();
        UpdateList();
    }
    if (add_newkez) {
        var kezobj = {
            s0: [0.6, 0.6],
            s1: [0.5, 0.5],
            s2: [0.5, 0.5],
            r1pos: [-0.4, -4, 0],
            r2pos: [0, -8, 0],
            frameOffset: 0,
            offsetPos: point,
            offsetToJoinPos: 0.8,
            offsetToTestPos: [0.7, 0.1],
            joinTestIndex: testindex
        };
        DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER].kez.push(kezobj);
        add_newkez = false;
        reSetAnimator();
        UpdateList();
    }
    if ($("chbox_szinv").checked) {
        var x = (tcoord[0] * 512) | 0;
        var y = (tcoord[1] * 512) | 0;
        var ret = paintCTX.getImageData(x, y, 1, 1);
        SetScrollbar("sett_festr", ret.data[0]);
        SetScrollbar("sett_festg", ret.data[1]);
        SetScrollbar("sett_festb", ret.data[2]);

        paintCTX.fillStyle = "rgb(" + $("sett_festr").value + "," + $("sett_festg").value + "," + $("sett_festb").value + ")";
    } else if ($("chbox_fest").checked) {

        var x = (tcoord[0] * 512) | 0;
        var y = (tcoord[1] * 512) | 0;
        var m = $("sett_festm").value;
        paintCTX.fillRect(x - m, y - m, m * 2, m * 2);
        var tex = karakter1.getTexture(kijelolType, kijelolIndex);
        pve.texman.drawTex(tex, paintCanvas);

        if ($("chbox_festpair").checked && arr.pair !== undefined && arr.pair >= 0) {
            var tex = karakter1.getTexture(kijelolType, arr.pair);
            if (tex === undefined) {
                tex = pve.texman.getfreetex();
                karakter1.setTexture(kijelolType, arr.pair, tex);
            }
            pve.texman.drawTex(tex, paintCanvas);
        }

    } else {
        if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {

            arr.offsetPos = point;
            arr.joinTestIndex = testindex;
            reSetAnimator();
        }
    }


}

var colladaData;

function loadDAE_cb(data) {
    if (colladaObj)
        colladaObj.clean();
    colladaData = pve.Load_Collada(data);

    //find tex in texCollector
    for (var i = 0; i < colladaData.meshlist.length; i++) {
        var mesh = colladaData.meshlist[i];
        if (mesh.effect && mesh.effect.img) {
            var teximg = mesh.effect.img;
            teximg.texobj = texCollector[teximg.src];
        }
    }

    colladaObj = new pve.animator.animatorInstance(colladaData);
    /*
     var vegtagindex;
     vegtagindex = colladaObj.addVegtag(pve.animator.VEGTAGTYPE_LIST.LAB);
     
     colladaObj.setVegtagProp(vegtagindex, "TAG1", "Bone_015");
     colladaObj.setVegtagProp(vegtagindex, "TAG2", "Bone_016");
     colladaObj.setVegtagProp(vegtagindex, "TAG3", "Bone_017");
     colladaObj.setVegtagProp(vegtagindex, "RANGLE", Math.PI / 2);
     
     
     vegtagindex = colladaObj.addVegtag(pve.animator.VEGTAGTYPE_LIST.LAB);
     colladaObj.setVegtagProp(vegtagindex, "TAG1", "Bone_018");
     colladaObj.setVegtagProp(vegtagindex, "TAG2", "Bone_019");
     colladaObj.setVegtagProp(vegtagindex, "TAG3", "Bone_020");
     colladaObj.setVegtagProp(vegtagindex, "OFFSET", 0.5);
     colladaObj.setVegtagProp(vegtagindex, "RANGLE", Math.PI / 2);
     
     vegtagindex = colladaObj.addVegtag(pve.animator.VEGTAGTYPE_LIST.KEZ);
     colladaObj.setVegtagProp(vegtagindex, "TAG1", "Bone_008");
     colladaObj.setVegtagProp(vegtagindex, "TAG2", "Bone_009");
     colladaObj.setVegtagProp(vegtagindex, "TAG3", "Bone_010");
     colladaObj.setVegtagProp(vegtagindex, "OFFSET", 0.5);
     colladaObj.setVegtagProp(vegtagindex, "RANGLE", -Math.PI / 2);
     colladaObj.setVegtagProp(vegtagindex, "VANGLE", Math.PI / 2 - 0.1);*/

    colladaObj.allinit();

}

function loadDAE() {

    pve.load.opendialog(loadDAE_cb);

}

function drawTerrain(now_render_framebuffer, renderpos, nezszog) {

    gl_.enable(gl_.CULL_FACE);
    gl_.mvPush();
    vvilag.Draw(false);
    gl_.mvPop();
    gl_.disable(gl_.CULL_FACE);

    pp += 0.1;

    if (colladaObj) {


        colladaObj.incAnimIndex(6);
        colladaObj.step();


        colladaObj.rebuild();
    }

    for (var i = 0; i < obj3dList.length; i++) {
        var obj = obj3dList[i];
        obj.incAnimIndex(6);
        obj.step();


        obj.rebuild();
    }


    if (now_axisraypick !== undefined && icontrol.key(INPUTKEYS.RBUTTON)) {


        var ret = now_axisraypick.mouseMove();
        if (ret !== undefined) {
            var arr = getDefanimatorSelected();
            vec3.sub(ret, ret, karakter1.getPos());
            vec3.scale(ret, ret, 1 / karakter1.getScale());
            if (now_axisraypick_index === 1 && kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB) {
                ret[1] = 0;
            }
            if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.LAB || kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.KEZ) {
                if (now_axisraypick_index === 0) {
                    arr.r1pos = vec3.sub([0, 0, 0], ret, arr.offsetPos);

                } else {
                    arr.r2pos = vec3.sub([0, 0, 0], ret, arr.offsetPos);
                }
            } else if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.TEST) {
                ret[2] = 0;
                arr.pos = vec3.copy([0, 0, 0], ret);

            } else if (kijelolType === PokolVoxelEngine_animator.KARAKTER_OBJTYPE.FEJ) {
                ret[2] = 0;
                arr.pos = vec3.copy([0, 0, 0], ret);

            }
            reSetAnimator();


            setArrowPos(now_axisraypick_index, ret);
        }

    } else {

        var needDataObj = {needElement: 1, needPickTexcoord: 1};

        var retraypick = getRaypPickIndex();
        var rp = undefined;
        if (retraypick !== undefined) {
            rp = karakter1.rayPick(retraypick[0], retraypick[1], needDataObj);
        }
        if (rp === undefined) {
            sphere.setHidden(true);

        } else {
            sphere.setPos(rp);
            sphere.setHidden(false);

            if (icontrol.key(INPUTKEYS.RBUTTON)) {
                globalpos = rp;
                var vnum = 21;
                var testIndexloc = [(needDataObj.needElement[0] / vnum) | 0, (needDataObj.needElement[1] / vnum) | 0, (needDataObj.needElement[2] / vnum) | 0];

                var joinindex = testIndexloc[0];
                joinindex = (testIndexloc[1] < joinindex) ? testIndexloc[1] : joinindex;
                joinindex = (testIndexloc[2] < joinindex) ? testIndexloc[2] : joinindex;
                joinindex--;
                raypickClick(joinindex, rp, needDataObj.needPickTexcoord);
            }
        }
    }
    pve.geos.draw();
    var animspeed = parseInt($("sett_animspeed").value) * $("sett_animspeed").factor;
    pve.animator.step(animspeed);
    pve.animator.draw();

}

function frameDraw() {


    pve.clear([0.5, 0.5, 0.8, 1.0]);
    pve.updateCamera(icontrol, [mopos.x, mopos.y]);



}

function KeyTick() {
    icontrol.start();
    var speed = 0.05;
    if (icontrol.key(INPUTKEYS.KEY_SHIFT))
        speed = 0.5;
    if (icontrol.key(INPUTKEYS.KEY_W)) {
        icontrol.MoveCam(icontrol.nezszog[0], icontrol.nezszog[1], speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_S)) {
        icontrol.MoveCam(icontrol.nezszog[0], icontrol.nezszog[1], -speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_D)) {
        icontrol.MoveCam(0, icontrol.nezszog[1] + 90, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_A)) {
        icontrol.MoveCam(0, icontrol.nezszog[1] - 90, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_SPACE)) {
        icontrol.MoveCam(-90, 0, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_CTRL)) {
        icontrol.MoveCam(90, 0, speed);
    }
    icontrol.end();
}

var fps = 0;
var frametime;
var lowframetime;

function frameTick() {
    var akttime = performance.now();
    if (lowframetime === 0 || lowframetime < (akttime - frametime))
        lowframetime = akttime - frametime;
    frametime = akttime;

    fps++;


    KeyTick();
    frameDraw();


}
function kiirFPS() {
    var perfstr = (((lowframetime * 1000) | 0) / 1000);
    $("fps").innerHTML = "FPS: " + fps + "<BR>usec:" + perfstr;
    lowframetime = 0;
    fps = 0;
    osszdraw = 0;
}