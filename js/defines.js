/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function BlockType(id, name, url, objname, csoport, csindex, leiras, tul) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.objname = objname;
    this.csoport = csoport;
    this.csoportindex = csindex;
    this.leiras = leiras;
    this.tul = tul;
    this.taxatlas_index = -1;//csak textúra atlas használatakor, atlas helyét mutatja majd meg
}
function CsoportType(id, name) {
    this.id = id;
    this.name = name;
    this.elemek = [];
    this.tul = {
        fenyblockade: 0,
        vizalpha: 0,
        texatlas: 0,
        obj3d: 0,
        pontfeny_rgb: 0,
        szabadfeny: 0,
    };
}


var BlockTypeList = [];
var CsoportTypeList = [];

function NewCsoportType(id, name, tullist) {
    CsoportTypeList[id] = new CsoportType(id, name);
    for (var i = 0; i < tullist.length; i++) {
        CsoportTypeList[id].tul[tullist[i]] = 1;
    }
}
function AddCsoportElem(csid, bid) {
    var cselem = CsoportTypeList[csid];
    return cselem.elemek.push(bid) - 1;

}
function NewBlockType(id, name, url, objname, csoport, leiras) {
    var ret = AddCsoportElem(csoport, id);
    BlockTypeList[id] = new BlockType(id, name, url, objname, csoport, ret, leiras, CsoportTypeList[csoport].tul);

}

NewCsoportType(1, "alapterrain", ["texatlas", "fenyblockade"]);

NewBlockType(1, "terep fű", "ground_grass_gen_05.png", "", 1, "Fű");

var INPUTKEYS = {
    KEY_W: 87,
    KEY_A: 65,
    KEY_S: 83,
    KEY_D: 68,
    KEY_0: 0x30,
    KEY_1: 0x31,
    KEY_2: 0x32,
    KEY_3: 0x33,
    KEY_4: 0x34,
    KEY_5: 0x35,
    KEY_Q: 81,
    KEY_T: 84,
    KEY_E: 0x45,
    KEY_SPACE: 32,
    KEY_CTRL: 17,
    KEY_SHIFT: 16,
    LBUTTON: 0,
    MBUTTON: 1,
    RBUTTON: 2
};
var DefAnimator = {};
DefAnimator.ANIMALG_ALL = 0;
DefAnimator.ANIMALG_JAR = 1;
DefAnimator.ANIMALG_S_FEGY1 = 2;
DefAnimator.ANIMALG_E_FEGY1 = 3;

function defConstans(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        writable: false,
        configurable: false
    });
}



DefAnimator.AnimAlg_List = [];
DefAnimator.AnimAlg_List[DefAnimator.ANIMALG_ALL] = [
    function (param) {

        return {lab_fok: 0, lab_tav: param.lab_tav, step: 60, talp: [-10, 0], kez_fok: 0, kez_tav: param.kez_tav};
    }
];
DefAnimator.AnimAlg_List[DefAnimator.ANIMALG_JAR] = [
    function (param) {

        return {lab_fok: 0, lab_tav: param.lab_tav, step: 60, talp: [-10, 0], kez_fok: 0, kez_tav: param.kez_tav * 0.9};
    },
    function (param) {

        return {lab_fok: -19, lab_tav: param.lab_tav, step: 60, talp: [-10, 0], kez_fok: -18, kez_tav: param.kez_tav * 0.9};
    },
    function (param) {

        return {lab_fok: -23, lab_tav: param.lab_tav * 0.95, step: 10, talp: [-10, 5], kez_fok: -23, kez_tav: param.kez_tav * 0.9};
    },
    function (param) {

        return {lab_fok: -19, lab_tav: param.lab_tav * 0.8, step: 30, talp: [-10, 5], kez_fok: -19, kez_tav: param.kez_tav * 0.9};
    },
    function (param) {

        return {lab_fok: 25, lab_tav: param.lab_tav * 0.95, step: 100, talp: [-10, 0], kez_fok: 20, kez_tav: param.kez_tav * 0.9};
    },
    function (param) {

        return {lab_fok: 7, lab_tav: param.lab_tav * 0.95, step: 60, talp: [-10, 2], kez_fok: 15, kez_tav: param.kez_tav * 0.9};
    }
];

DefAnimator.AnimAlg_List[DefAnimator.ANIMALG_S_FEGY1] = [
    function (param) {

        return {lab_fok: 0, lab_tav: param.lab_tav, step: 60, talp: [-10, 0], kez_fok: 0, kez_tav: param.kez_tav * 0.9};
    }
];
DefAnimator.TYPEDATA_EMBER = 0;
DefAnimator.TYPEDATA_POK = 1;
DefAnimator.model_list = [];
DefAnimator.model_list[DefAnimator.TYPEDATA_EMBER] = {
    fej: [/*
     {
     pos: [-0.2, 0, 0],
     size: [0.6, 0.6]
     },
     {
     pos: [0, 0.3, 0],
     size: [1, 0.8]
     },
     {
     pos: [0, 0.7, 0],
     size: [1, 0.8]
     },
     {
     pos: [0, 1.3, 0],
     size: [1, 0.8]
     },
     {
     pos: [0, 1.7, 0],
     size: [0.6, 0.4]
     },
     {
     pos: [0, 1.8, 0],
     size: [0, 0]
     }*/

    ],
    kez: [/*
     {
     s0: [0.6, 0.6],
     s1: [0.5, 0.5],
     s2: [0.5, 0.5],
     r1pos: [-0.4, -3, 1],
     r2pos: [0, -6, 2],
     frameOffset: 0.5,
     offsetPos: [0, 16, 1.5],
     offsetToJoinPos: 0.8,
     offsetToTestPos: [0.7, 0.1],
     joinTestIndex: 1
     
     },
     {
     s0: [0.6, 0.6],
     s1: [0.5, 0.5],
     s2: [0.5, 0.5],
     r1pos: [-0.4, -4, 0],
     r2pos: [0, -8, 0],
     frameOffset: 0,
     offsetPos: [0, 15, -2.5],
     offsetToJoinPos: 0.8,
     offsetToTestPos: [0.7, 0.1],
     joinTestIndex: 1
     }*/
    ],
    test: [
        {
            pos: [0, 10, 0],
            size: [1, 1.8],
            rot: undefined
        },
        {
            pos: [0, 12, 0],
            size: [1, 1.4],
            rot: undefined
        },
        {
            pos: [0, 16, 0],
            size: [1, 2],
            rot: undefined
        },
        {
            pos: [-0.2, 16.5, 0],
            size: [0.6, 0.6],
            rot: undefined
        },
        {
            pos: [-0.2, 17, 0],
            size: [0.6, 0.6],
            rot: undefined
        }
    ],
    lab: [
        {
            name: "",
            s0: [0.8, 0.8],
            s1: [0.8, 0.8],
            s2: [0.8, 0.8],
            r1pos: [0.1, -5, 0],
            r2pos: [0, -10, 0],
            frameOffset: 0.5,
            offsetPos: [0, 10, -0.9],
            offsetToJoinPos: 0.8,
            offsetToTestPos: [0.7, 0.1],
            joinTestIndex: -1
        },
        {
            name: "",
            s0: [0.8, 0.8],
            s1: [0.8, 0.8],
            s2: [0.8, 0.8],
            r1pos: [0.1, -5, 0],
            r2pos: [0, -10, 0],
            frameOffset: 0,
            offsetPos: [0, 10, 0.9],
            offsetToJoinPos: 0.8,
            offsetToTestPos: [0.7, 0.1],
            joinTestIndex: -1
        }
    ]
};
DefAnimator.model_list[DefAnimator.TYPEDATA_POK] = {
    fej: [
    ],
    kez: [
    ],
    test: [
    ],
    lab: [/*
     {
     s0: [1, 1],
     s1: [1, 1],
     s2: [1, 1],
     r1pos: [-5, 0, 10],
     r2pos: [-10, -20, 20],
     frameOffset: 0,
     offsetPos: [0, 0, 2],
     joinTestIndex: -1
     },
     {
     s0: [1, 1],
     s1: [1, 1],
     s2: [1, 1],
     r1pos: [-5, 0, -10],
     r2pos: [-10, -20, -20],
     frameOffset: 0.5,
     offsetPos: [0, 0, -2],
     joinTestIndex: -1
     },
     {
     s0: [1, 1],
     s1: [1, 1],
     s2: [1, 1],
     r1pos: [0, 0, 10],
     r2pos: [0, -20, 20],
     frameOffset: 0.5,
     offsetPos: [5, 0, 2],
     joinTestIndex: -1
     },
     {
     s0: [1, 1],
     s1: [1, 1],
     s2: [1, 1],
     r1pos: [0, 0, -10],
     r2pos: [0, -20, -20],
     frameOffset: 0,
     offsetPos: [5, 0, -2],
     joinTestIndex: -1
     },
     {
     s0: [1, 1],
     s1: [1, 1],
     s2: [1, 1],
     r1pos: [5, 0, 10],
     r2pos: [10, -20, 20],
     frameOffset: 0,
     offsetPos: [10, 0, 2],
     joinTestIndex: -1
     },
     {
     s0: [1, 1],
     s1: [1, 1],
     s2: [1, 1],
     r1pos: [5, 0, -10],
     r2pos: [10, -20, -20],
     frameOffset: 0.5,
     offsetPos: [10, 0, -2],
     joinTestIndex: -1
     }*/
    ]
};